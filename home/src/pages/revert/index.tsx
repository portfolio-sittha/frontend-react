import * as yup from 'yup'

import {
  Factorialize,
  PrimeNumberCalculator,
  RevertString,
  User
} from '@app/models'

import { Container } from '@app/components'
import { TextField } from '@app/components/common/TextField'
import { useForm } from 'react-hook-form'
import { useState } from 'react'
import { yupResolver } from '@hookform/resolvers/yup/dist/yup'

const validationSchema = yup
  .object()
  .shape({
    value: yup.string().required('value is required')
  })
  .required()

function revertString(val: string) {
  var splitString = val.split('')

  return splitString.reverse().join('')
}

const Revert: React.FC = () => {
  const [response, setResponse] = useState<boolean>(false)

  const onSubmit = (data) => {
    const res = revertString(data.value)

    data.value === res ? setResponse(true) : setResponse(false)

    return false
  }

  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm<RevertString>({
    mode: 'onTouched',
    resolver: yupResolver(validationSchema)
  })

  return (
    <Container>
      <div className="flex flex-col justify-center items-start max-w-2xl border-gray-200 dark:border-gray-700 mx-auto pb-16">
        <div className="w-full max-w-xs">
          <form
            onSubmit={handleSubmit(onSubmit)}
            className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
            <div className="mb-4">
              <TextField
                label="Value"
                name="value"
                register={register('value')}
              />
              {errors.value && (
                <p className="text-red-500 text-xs italic">
                  {errors.value.message}
                </p>
              )}
            </div>
            <div className="flex items-center justify-between">
              <button
                className="w-full bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                type="submit">
                Calculator
              </button>
            </div>
          </form>
        </div>
        <div className="flex flex-col justify-center items-start max-w-2xl border-gray-200 dark:border-gray-700 mx-auto pb-16">
          <div className="w-full max-w-xs">
            <div className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
              <p className="break-words text-green-500 text-xs">
                {response && `คำศัพท์ถูกต้องตามเงื่อนไข`}
              </p>
            </div>
          </div>
        </div>

        <span className="h-16" />
      </div>
    </Container>
  )
}

export default Revert
