import * as yup from 'yup'

import { Factorialize, PrimeNumberCalculator, User } from '@app/models'

import { Container } from '@app/components'
import { TextField } from '@app/components/common/TextField'
import { useForm } from 'react-hook-form'
import { useState } from 'react'
import { yupResolver } from '@hookform/resolvers/yup/dist/yup'

const validationSchema = yup
  .object()
  .shape({
    number: yup.number().required('number is required')
  })
  .required()

function factorialize(num: number) {
  if (num === 0 || num === 1) return 1
  for (var i = num - 1; i >= 1; i--) {
    num *= i
  }
  return num
}

export default function FactorializePage() {
  const [response, setResponse] = useState<any>()

  const onSubmit = (data) => {
    console.log(data.number)
    const res = factorialize(data.number)

    setResponse(res)

    return false
  }

  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm<Factorialize>({
    mode: 'onTouched',
    resolver: yupResolver(validationSchema)
  })

  return (
    <Container>
      <div className="flex flex-col justify-center items-start max-w-2xl border-gray-200 dark:border-gray-700 mx-auto pb-16">
        <div className="w-full max-w-xs">
          <form
            onSubmit={handleSubmit(onSubmit)}
            className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
            <div className="mb-4">
              <TextField
                label="Number"
                name="number"
                register={register('number')}
              />
              {errors.number && (
                <p className="text-red-500 text-xs italic">
                  {errors.number.message}
                </p>
              )}
            </div>
            <div className="flex items-center justify-between">
              <button
                className="w-full bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                type="submit">
                Calculator
              </button>
            </div>
          </form>
        </div>
        <div className="flex flex-col justify-center items-start max-w-2xl border-gray-200 dark:border-gray-700 mx-auto pb-16">
          <div className="w-full max-w-xs">
            <div className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
              <p className="break-words text-red-500 text-xs">
                {response && `${response}`}
              </p>
            </div>
          </div>
        </div>

        <span className="h-16" />
      </div>
    </Container>
  )
}
