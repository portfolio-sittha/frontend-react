import { NextApiRequest, NextApiResponse } from 'next'

const handler = async (req: NextApiRequest, res: NextApiResponse<any>) => {
  try {
    return res.status(200).json({
      Code: 100,
      Description: 'วันที่ 16 มกนาคม 2563 เวลา 11:59 น',
      IsNotify: true,
      IsChange: false,
      ChangeDate: '16/1/2564',
      ListOfPrice: [
        {
          MAT_NAME: 'GASOHOL 95 E-10 WITH ADD.',
          DIVISION_ID: 10,
          DIVISION_NAME: 'Light oil',
          MAT_NAM2: 'GSHOS',
          TYPE_NAME: 2,
          MAT_ID: '500018',
          PRICE0: 25.85
        }
      ]
    })
  } catch (e) {
    console.log(e)
    return res.status(500).json({ error: 'Unexpected error.' })
  }
}

export default handler
