import GoogleProvider from 'next-auth/providers/google'
import NextAuth from 'next-auth'
import { PrismaAdapter } from '@next-auth/prisma-adapter'
import { PrismaClient } from '@prisma/client'

const prisma = new PrismaClient()

const google_client_id = process.env.GOOGLE_CLIENT_ID
  ? process.env.GOOGLE_CLIENT_ID
  : ''

const google_client_secret = process.env.GOOGLE_CLIENT_SECRET
  ? process.env.GOOGLE_CLIENT_SECRET
  : ''

export default NextAuth({
  adapter: PrismaAdapter(prisma),
  providers: [
    GoogleProvider({
      clientId: google_client_id,
      clientSecret: google_client_secret
    })
  ]
})
