import * as yup from 'yup'

import {
  Factorialize,
  PrimeNumberCalculator,
  RevertString,
  User
} from '@app/models'

import { Container } from '@app/components'
import { TextField } from '@app/components/common/TextField'
import { useForm } from 'react-hook-form'
import { useState } from 'react'
import { yupResolver } from '@hookform/resolvers/yup/dist/yup'

const validationSchema = yup
  .object()
  .shape({
    num: yup.number().required('value is required')
  })
  .required()

function dump(num: number) {
  const element = []
  for (let index = 0; index <= num; index++) {
    element.push(`${1}`)
  }

  return element.join('')
}

const Runner: React.FC = () => {
  const [response, setResponse] = useState<string[]>([])

  const onSubmit = (data) => {
    const element = []
    for (let index = 0; index <= data.num; index++) {
      element.push(dump(index))
    }
    setResponse(element)
    return false
  }

  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm<{ num: number }>({
    mode: 'onTouched',
    resolver: yupResolver(validationSchema)
  })

  return (
    <Container>
      <div className="flex flex-col justify-center items-start max-w-2xl border-gray-200 dark:border-gray-700 mx-auto pb-16">
        <div className="w-full max-w-xs">
          <form
            onSubmit={handleSubmit(onSubmit)}
            className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
            <div className="mb-4">
              <TextField label="Number" name="num" register={register('num')} />
              {errors.num && (
                <p className="text-red-500 text-xs italic">
                  {errors.num.message}
                </p>
              )}
            </div>
            <div className="flex items-center justify-between">
              <button
                className="w-full bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                type="submit">
                Calculator
              </button>
            </div>
          </form>
        </div>
        <div className="flex flex-col justify-center items-start max-w-2xl border-gray-200 dark:border-gray-700 mx-auto pb-16">
          <div className="w-full max-w-xs">
            <div className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
              {response.length > 0 &&
                response.map((v, i) => (
                  <p className="break-words text-green-500 text-xs">{v}</p>
                ))}
            </div>
          </div>
        </div>

        <span className="h-16" />
      </div>
    </Container>
  )
}

export default Runner
