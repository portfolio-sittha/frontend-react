import * as yup from 'yup'

import { PrimeNumberCalculator, User } from '@app/models'

import { Container } from '@app/components'
import { TextField } from '@app/components/common/TextField'
import { useForm } from 'react-hook-form'
import { useState } from 'react'
import { yupResolver } from '@hookform/resolvers/yup/dist/yup'

const validationSchema = yup
  .object()
  .shape({
    start: yup.number().required('start number is required'),
    end: yup.number().required('end number is required')
  })
  .required()

function isPrime(num) {
  for (var i = 2; i < num; i++) if (num % i === 0) return false
  return num > 1
}

export default function Home() {
  const [response, setResponse] = useState<number[]>([])

  const onSubmit = (data) => {
    const res: number[] = []

    for (let i = data.start; i <= data.end; i++) {
      isPrime(i) && res.push(i)
    }

    console.log(res)

    setResponse(res)

    return false
  }

  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm<PrimeNumberCalculator>({
    mode: 'onTouched',
    resolver: yupResolver(validationSchema)
  })

  return (
    <Container>
      <div className="flex flex-col justify-center items-start max-w-2xl border-gray-200 dark:border-gray-700 mx-auto pb-16">
        <div className="w-full max-w-xs">
          <form
            onSubmit={handleSubmit(onSubmit)}
            className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
            <div className="mb-4">
              <TextField
                label="Start Number"
                name="start"
                register={register('start')}
              />
              {errors.start && (
                <p className="text-red-500 text-xs italic">
                  {errors.start.message}
                </p>
              )}
            </div>
            <div className="mb-6">
              <TextField
                label="End Number"
                name="end"
                register={register('end')}
              />
              {errors.end && (
                <p className="text-red-500 text-xs italic">
                  {errors.end.message}
                </p>
              )}
            </div>
            <div className="flex items-center justify-between">
              <button
                className="w-full bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                type="submit">
                Calculator
              </button>
            </div>
          </form>
        </div>
        <div className="flex flex-col justify-center items-start max-w-2xl border-gray-200 dark:border-gray-700 mx-auto pb-16">
          <div className="w-full max-w-xs">
            <div className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
              <p className="break-words text-red-500 text-xs">
                {response.length > 0 && response.map((v, i) => `${v},`)}
              </p>
            </div>
          </div>
        </div>

        <span className="h-16" />
      </div>
    </Container>
  )
}
