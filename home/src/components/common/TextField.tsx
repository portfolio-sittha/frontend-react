import { UseFormRegister, UseFormRegisterReturn } from 'react-hook-form'

type TextFieldProps = {
  label: string
  name: string
  register: UseFormRegisterReturn
}

const TextField: React.FC<TextFieldProps> = ({ label, name, register }) => {
  return (
    <>
      <label
        className="block text-gray-700 text-sm font-bold mb-2"
        htmlFor={name}>
        {label}
      </label>
      <input
        {...register}
        className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 dark:text-white leading-tight focus:outline-none focus:shadow-outline"
      />
    </>
  )
}

export { TextField }
