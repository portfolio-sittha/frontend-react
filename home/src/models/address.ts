type Address = {
  address1: string
  street: string
  district: string
  city: string
  state: string
  zipCode: number
}

export type { Address }
