type PrimeNumberCalculator = {
  start: number
  end: number
}

export type { PrimeNumberCalculator }
