type Factorialize = {
  number: number
}

type RevertString = {
  value: string
}

export type { Factorialize, RevertString }
