"use strict";
/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(() => {
var exports = {};
exports.id = "pages/api/GetListOfPriceData";
exports.ids = ["pages/api/GetListOfPriceData"];
exports.modules = {

/***/ "./src/pages/api/GetListOfPriceData/index.ts":
/*!***************************************************!*\
  !*** ./src/pages/api/GetListOfPriceData/index.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\nconst handler = async (req, res) => {\n  try {\n    return res.status(200).json({\n      Code: 100,\n      Description: 'วันที่ 16 มกนาคม 2563 เวลา 11:59 น',\n      IsNotify: true,\n      IsChange: false,\n      ChangeDate: '16/1/2564',\n      ListOfPrice: [{\n        MAT_NAME: 'GASOHOL 95 E-10 WITH ADD.',\n        DIVISION_ID: 10,\n        DIVISION_NAME: 'Light oil',\n        MAT_NAM2: 'GSHOS',\n        TYPE_NAME: 2,\n        MAT_ID: '500018',\n        PRICE0: 25.85\n      }]\n    });\n  } catch (e) {\n    console.log(e);\n    return res.status(500).json({\n      error: 'Unexpected error.'\n    });\n  }\n};\n\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (handler);//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvcGFnZXMvYXBpL0dldExpc3RPZlByaWNlRGF0YS9pbmRleC50cy5qcyIsIm1hcHBpbmdzIjoiOzs7O0FBRUEsTUFBTUEsT0FBTyxHQUFHLE9BQU9DLEdBQVAsRUFBNEJDLEdBQTVCLEtBQTBEO0FBQ3hFLE1BQUk7QUFDRixXQUFPQSxHQUFHLENBQUNDLE1BQUosQ0FBVyxHQUFYLEVBQWdCQyxJQUFoQixDQUFxQjtBQUMxQkMsTUFBQUEsSUFBSSxFQUFFLEdBRG9CO0FBRTFCQyxNQUFBQSxXQUFXLEVBQUUsb0NBRmE7QUFHMUJDLE1BQUFBLFFBQVEsRUFBRSxJQUhnQjtBQUkxQkMsTUFBQUEsUUFBUSxFQUFFLEtBSmdCO0FBSzFCQyxNQUFBQSxVQUFVLEVBQUUsV0FMYztBQU0xQkMsTUFBQUEsV0FBVyxFQUFFLENBQ1g7QUFDRUMsUUFBQUEsUUFBUSxFQUFFLDJCQURaO0FBRUVDLFFBQUFBLFdBQVcsRUFBRSxFQUZmO0FBR0VDLFFBQUFBLGFBQWEsRUFBRSxXQUhqQjtBQUlFQyxRQUFBQSxRQUFRLEVBQUUsT0FKWjtBQUtFQyxRQUFBQSxTQUFTLEVBQUUsQ0FMYjtBQU1FQyxRQUFBQSxNQUFNLEVBQUUsUUFOVjtBQU9FQyxRQUFBQSxNQUFNLEVBQUU7QUFQVixPQURXO0FBTmEsS0FBckIsQ0FBUDtBQWtCRCxHQW5CRCxDQW1CRSxPQUFPQyxDQUFQLEVBQVU7QUFDVkMsSUFBQUEsT0FBTyxDQUFDQyxHQUFSLENBQVlGLENBQVo7QUFDQSxXQUFPaEIsR0FBRyxDQUFDQyxNQUFKLENBQVcsR0FBWCxFQUFnQkMsSUFBaEIsQ0FBcUI7QUFBRWlCLE1BQUFBLEtBQUssRUFBRTtBQUFULEtBQXJCLENBQVA7QUFDRDtBQUNGLENBeEJEOztBQTBCQSxpRUFBZXJCLE9BQWYiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9mcm9udGVuZC1yZWFjdC8uL3NyYy9wYWdlcy9hcGkvR2V0TGlzdE9mUHJpY2VEYXRhL2luZGV4LnRzP2IwOWMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmV4dEFwaVJlcXVlc3QsIE5leHRBcGlSZXNwb25zZSB9IGZyb20gJ25leHQnXG5cbmNvbnN0IGhhbmRsZXIgPSBhc3luYyAocmVxOiBOZXh0QXBpUmVxdWVzdCwgcmVzOiBOZXh0QXBpUmVzcG9uc2U8YW55PikgPT4ge1xuICB0cnkge1xuICAgIHJldHVybiByZXMuc3RhdHVzKDIwMCkuanNvbih7XG4gICAgICBDb2RlOiAxMDAsXG4gICAgICBEZXNjcmlwdGlvbjogJ+C4p+C4seC4meC4l+C4teC5iCAxNiDguKHguIHguJnguLLguITguKEgMjU2MyDguYDguKfguKXguLIgMTE6NTkg4LiZJyxcbiAgICAgIElzTm90aWZ5OiB0cnVlLFxuICAgICAgSXNDaGFuZ2U6IGZhbHNlLFxuICAgICAgQ2hhbmdlRGF0ZTogJzE2LzEvMjU2NCcsXG4gICAgICBMaXN0T2ZQcmljZTogW1xuICAgICAgICB7XG4gICAgICAgICAgTUFUX05BTUU6ICdHQVNPSE9MIDk1IEUtMTAgV0lUSCBBREQuJyxcbiAgICAgICAgICBESVZJU0lPTl9JRDogMTAsXG4gICAgICAgICAgRElWSVNJT05fTkFNRTogJ0xpZ2h0IG9pbCcsXG4gICAgICAgICAgTUFUX05BTTI6ICdHU0hPUycsXG4gICAgICAgICAgVFlQRV9OQU1FOiAyLFxuICAgICAgICAgIE1BVF9JRDogJzUwMDAxOCcsXG4gICAgICAgICAgUFJJQ0UwOiAyNS44NVxuICAgICAgICB9XG4gICAgICBdXG4gICAgfSlcbiAgfSBjYXRjaCAoZSkge1xuICAgIGNvbnNvbGUubG9nKGUpXG4gICAgcmV0dXJuIHJlcy5zdGF0dXMoNTAwKS5qc29uKHsgZXJyb3I6ICdVbmV4cGVjdGVkIGVycm9yLicgfSlcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBoYW5kbGVyXG4iXSwibmFtZXMiOlsiaGFuZGxlciIsInJlcSIsInJlcyIsInN0YXR1cyIsImpzb24iLCJDb2RlIiwiRGVzY3JpcHRpb24iLCJJc05vdGlmeSIsIklzQ2hhbmdlIiwiQ2hhbmdlRGF0ZSIsIkxpc3RPZlByaWNlIiwiTUFUX05BTUUiLCJESVZJU0lPTl9JRCIsIkRJVklTSU9OX05BTUUiLCJNQVRfTkFNMiIsIlRZUEVfTkFNRSIsIk1BVF9JRCIsIlBSSUNFMCIsImUiLCJjb25zb2xlIiwibG9nIiwiZXJyb3IiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./src/pages/api/GetListOfPriceData/index.ts\n");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../webpack-api-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__("./src/pages/api/GetListOfPriceData/index.ts"));
module.exports = __webpack_exports__;

})();